// import { gql } from '@apollo/client';
import gql from "graphql-tag";

// const GET_ALL_ARTICLES = gql`
//   query GetArticles($limit: Int, $start:Int, $where:JSON) {
//     pages {
//         articles(sort: "published_at" , limit:$limit, start:$start, where: $where) {
//             id
//             slug
//             contentSections {
//                 __typename
//                 ... on ComponentSectionsRichText {
//                   content
//                 }
//                 ... on ComponentSectionsArticleSection {
//                     title
//                     preamble
//                 }
//             }
//         }
//     }
//   }
// `;

// query all articles in the article collection
// const GET_ALL_ARTICLES = gql`
//   query GetArticles($limit: Int, $start:Int, $where:JSON) {
//     articles(sort: "published_at" , limit:$limit, start:$start, where: $where) {
//         id
//         slug
//         contentSections {
//                 __typename
//                 ... on ComponentSectionsRichText {
//                 articleText
//             }
//                 ... on ComponentSectionsArticleSection {
//                 title
//             }
//         }
//         categories {
//           id
//           title
//         }
//     }
//   }
// `; 

const GET_ALL_ARTICLES = gql`
  query GetArticles($limit: Int, $start:Int, $where:JSON) {
    articlereals(sort: "published_at" , limit:$limit, start:$start, where: $where) {
      id
      slug
      dynamiczone {
          ... on ComponentSectionsArticleSection {
          title
        }
          ... on ComponentSectionsPreamble {
            preamble
        }
          ... on ComponentSectionsRichText {
            articleText
        }
            ... on ComponentSectionsFacts {
          facts
        }
        ... on ComponentElementsFeatureImage{
          featuredImage {
            url
          }
        }
      }
      categories {
        title
      }
      featpic {
        url
      }
    }
  }
`;


export default GET_ALL_ARTICLES;

// import gql from "graphql-tag";

// const ARTICLES_QUERY = gql`
// query getArticle($limit: Int, $start:Int, $where:JSON) {
//   articles(sort: "published_at" , limit:$limit, start:$start, where: $where) {
//     id,
//     Title,
//     Slug,
//     Body, 
//     FeatureImage {
//       url
//     }
//     categories{
//       id
//       Name
//     }
//   }  
// }
// `;

// export default ARTICLES_QUERY;
