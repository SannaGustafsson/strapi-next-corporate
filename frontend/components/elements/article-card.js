import React from "react";
import Link from "next/link";

// Williams page to show articles and click them to read the full article
// featureImage
const ArticleCard = ({ title, articleText, categories, id }) => {
  const API_URL = process.env.NEXT_PUBLIC_API_URL || "http://localhost:1337"
//   const imageUrl =
//       process.env.NODE_ENV !== "development"
//         ? FeatureImage.url
//         : API_URL + FeatureImage.url;
  
  const summary = articleText.slice(0,100);

    return (
      <Link href={{ pathname: "articlereals", query: { id: id } }} >
        <div className="max-w rounded overflow-hidden shadow-lg">
            {/* <img className="w-full" src={imageUrl} alt={featureImage.url}/> */}
            <div className="p-10">
            <div className="font-bold text-xl mb-2">{title}</div>
            <p className="text-gray-700 text-base">
                {summary}
            </p>
            <div className="pt-4 pb-2 ">
            {categories.map((category) => {
                return (<span  key={`articlereals_${id}_${category.id}`} className="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">{ category.title }</span>);
            })}   
            </div>
            </div>
        </div>
      </Link>
    );
};

export default ArticleCard;
