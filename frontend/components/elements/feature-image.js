import { getStrapiMedia } from "utils/media";
import PropTypes from "prop-types";
import { mediaPropTypes } from "utils/types";

const FeatureImage = ({ media, className }) => {
  const { url, alternativeText } = media;
  console.log(media)
  const fullUrl = getStrapiMedia(url);

  return (
    <img src={fullUrl} alt={alternativeText || ""} className={className} />
  );
};

FeatureImage.propTypes = {
  media: mediaPropTypes.isRequired,
  className: PropTypes.string,
};

export default FeatureImage;