const FactBox = ({ data }) => {
    return (
        <div className="border-gray-300 border-2">
            <p className="text-textColor pt-2">{data}</p>
        </div>
    );
  };
  
  export default FactBox;