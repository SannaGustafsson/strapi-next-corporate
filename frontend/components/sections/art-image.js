import Image from "../elements/image";

const ArtImage = ({ data }) => {
  return (
    <section className="porse porse-lg container">
        <div className="flex-shrink-0 object-contain w-full mt-6">
        {/* <div className="w-5/6 mx-auto sm:9/12 lg:16/9 max-h-full"> */}
            <Image media={data.pic} className="w-full h-auto rounded-large" />
        </div>
    </section>
  );
};

export default ArtImage;