
const Preamble = ({ data }) => {
  return (
    <p className="text-textGray pt-2">{data}</p>
  );
};

export default Preamble;