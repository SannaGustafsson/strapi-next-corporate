import { useQuery, gql } from '@apollo/client';
import Markdown from "react-markdown";
import GET_ALL_ARTICLES from "../../apollo/queries/article/article-section"
import Image from "../elements/image";
import FeatureImage from "../elements/feature-image"
import Query from "../query";
import ArticleCard from "../elements/article-card";
 
function GetAllArticles() {
    const { loading, error, data } = useQuery(GET_ALL_ARTICLES);
  
    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;
        
    // const categor = data.articles.map((article) => (
    //         article.categories.map((category) => (
    //             <div>
    //                 <p>
    //                     {category.title}
    //                 </p>
    //             </div>
    //         ))
    //     ))
    // article.categories.map((category) => {

    // })

    // const { title, preamble, content }
    
    // data.articles.map((article) => {
    //         article.contentSections.map((contentSection) => {
    //             title = contentSection.title
    //             preamble = contentSection.preamble
    //             content = contentSection.content
    //         })
    //     })
    // let url = 'http://localhost:1337/uploads/article1_143f4933e6.jpg'

    return (
        // query all articles in the article collection
        <div className="container pt-4">
            {data.articlereals.map((article) => (
                <div className="flex flex-col w-auto">
                    <div className="bg-white rounded-large p-3 mb-4">
                    <Image media={article.featpic} className="w-full h-auto rounded-large" />
                        {article.categories.map((category) => (
                            <p className="flex lowercase font-semibold bg-gray-300">#{category.title}</p>
                        ))}
                        {article.dynamiczone.map((dynamic) => (
                            <div className="bg-white">
                                {/* <FeatureImage media={dynamic.featuredImage.url} className="w-full h-auto rounded-large" /> */}
                                {/* <img className="w-full" src={url} /> */}
                                    <h1 className="text-3xl tracking-wide font-bold text-textColor">{dynamic.title}</h1>
                                    <p className="text-textGray text-lg pt-2">{dynamic.preamble}</p>
                                <div className="">
                                    <p className="text-textColor pt-2 font-large">{dynamic.facts}</p>
                                </div>
                                {/* <div> */}
                                    <Markdown source={dynamic.articleText} />
                                {/* </div> */}
                            </div>

                        ))}
                    </div>

                </div>
            ))}
        </div>
    )
}

const ArticleSection = ({ data }) => {
  return <GetAllArticles />

//   let { categories } = data;
//   console.log(categories)
// //   skip = parseInt(skip);
// //   items = parseInt(items);

//   const categoriesArray = [];
//   categories.map((category) => {
//     let { title } = category.title
//     categoriesArray.push(title);
//    }
//   );
// //   let where = {categories: { title: categoriesArray  }};

// //   const gridClassName = `grid grid-cols-1 sm:grid-cols-${items} md:grid-cols-${items} lg:grid-cols-${items} xl:grid-cols-${items} gap-4 gap-y-2`;
  
//   return (
//     <div className="container mx-auto">
//       {/* <div className={gridClassName}> */}  {/* limit={items} */} 
//         <Query query={GET_ALL_ARTICLES} where={where}> 
//           {({ data: { articlereals } }) => {
//             return articlereals.map(({ id, title, articleText, preamble, facts, categories, featureImage }) => (
//               <ArticleCard title={title} articleText={articleText} preamble={preamble} facts={facts} categories={categories} featureImage={featureImage} id={id} key={`articlereals_${id}`} />
//             )); 
//           }}
//         </Query>
//       {/* </div> */}
//     </div>
//   );
};

export default ArticleSection;

// Use after restructuring Strapi with categories etc
// import Query from "../query";

// import ARTICLES_QUERY from "../../apollo/queries/article/article-section"
// import ArticleCard from "../elements/article-card";

// const ArticleSection = ({ data }) => {
//   let {categories} = data;
//   skip = parseInt(skip);
//   items = parseInt(items);

//   const categoriesArray = [];
//   categories.map(category => {
//     let {title} = category;
//     categoriesArray.push(title);
//    }
//   );
//   let where = {categories: { title: categoriesArray  }};

//   const gridClassName = `grid grid-cols-1 sm:grid-cols-${items} md:grid-cols-${items} lg:grid-cols-${items} xl:grid-cols-${items} gap-4 gap-y-2`;
  
//   return (
//     <div className="container mx-auto">
//       <div className={gridClassName}>
//         <Query query={GET_ALL_ARTICLES} limit={items} where={where}> 
//           {({ data: { articles } }) => {
//             return articles.map(({ id, title, articleText, preamble, facts, categories, featureImage }) => (
//               <ArticleCard title={title} articleText={articleText} preamble={preamble} facts={facts} categories={categories} featureImage={featureImage} id={id} key={`articlereal_${id}`} />
//             )); 
//           }}
//         </Query>
//       </div>
//     </div>
//   );
// };

// export default ArticleSection;
