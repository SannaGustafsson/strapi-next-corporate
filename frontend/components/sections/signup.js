import { getButtonAppearance } from "utils/button";
import ButtonLink from "../elements/button-link";
import { MdCheckBox } from "react-icons/md";

const Signup = ({ data }) => {
  return (
    <div>
        <div className="container w-4/6 h-auto mt-10 p-8 bg-white rounded-md">
            <div className="mb-5">
                <div className="border-gray-200 border-2 rounded-md p-6">
                    <div className="flex flex-col-2 justify-between">
                        <div className="flex flex-col">
                            <p className="font-bold">Skapa ett Life Of Svea konto</p>
                            <p className="tracking-wider uppercase text-textGray text-xs font-medium">Fortsätt till familjeliv</p>
                        </div>
                        {/* svg has a margin-top that is not supposed to be there */}
                        <svg className="mt-2" width="101" height="27" viewBox="0 0 101 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M10.4152 6.01658H4.72543V9.14917H9.90893V13.2762H4.72543V20.6354H0V1.8895H10.4152V6.01658Z" fill="#241636"/>
                            <path d="M14.7788 14.221C14.7788 14.6354 14.8512 15.0249 14.9958 15.3895C15.1405 15.7376 15.3333 16.0442 15.5744 16.3094C15.8316 16.5746 16.129 16.7818 16.4665 16.9309C16.8201 17.0801 17.1978 17.1547 17.5996 17.1547C17.9854 17.1547 18.347 17.0801 18.6845 16.9309C19.0381 16.7818 19.3355 16.5746 19.5766 16.3094C19.8338 16.0442 20.0347 15.7376 20.1793 15.3895C20.34 15.0414 20.4204 14.6685 20.4204 14.2707C20.4204 13.8729 20.34 13.5 20.1793 13.1519C20.0347 12.7873 19.8338 12.4724 19.5766 12.2072C19.3355 11.942 19.0381 11.7348 18.6845 11.5856C18.347 11.4365 17.9854 11.3619 17.5996 11.3619C17.1978 11.3619 16.8201 11.4365 16.4665 11.5856C16.129 11.7348 15.8316 11.942 15.5744 12.2072C15.3333 12.4724 15.1405 12.779 14.9958 13.1271C14.8512 13.4586 14.7788 13.8232 14.7788 14.221ZM20.2758 7.88122H24.6637V20.6354H20.2758V19.2182C19.3435 20.4282 18.0818 21.0331 16.4906 21.0331C15.5905 21.0331 14.7628 20.8674 14.0073 20.5359C13.2519 20.1878 12.5929 19.7072 12.0304 19.0939C11.4678 18.4807 11.0258 17.7597 10.7044 16.9309C10.399 16.1022 10.2463 15.1989 10.2463 14.221C10.2463 13.3094 10.399 12.4475 10.7044 11.6354C11.0097 10.8066 11.4357 10.0856 11.9821 9.47238C12.5286 8.85912 13.1796 8.37845 13.935 8.03039C14.6904 7.66574 15.5262 7.48342 16.4424 7.48342C17.9854 7.48342 19.2632 8.03867 20.2758 9.14917V7.88122Z" fill="#241636"/>
                            <path d="M25.7045 7.88122H30.0683V9.44751C31.2577 8.27072 32.5917 7.68232 34.0704 7.68232C35.9027 7.68232 37.3011 8.4779 38.2655 10.0691C39.2459 8.46133 40.6442 7.65746 42.4605 7.65746C43.0391 7.65746 43.5856 7.74862 44.0999 7.93094C44.6143 8.09668 45.0563 8.37016 45.4259 8.75138C45.7956 9.11602 46.0849 9.59669 46.2939 10.1934C46.5189 10.7901 46.6314 11.5111 46.6314 12.3564V20.6354H42.2676V14.3453C42.2676 13.6989 42.2194 13.1685 42.123 12.7541C42.0426 12.3398 41.914 12.0166 41.7372 11.7845C41.5765 11.5525 41.3836 11.395 41.1586 11.3122C40.9336 11.2293 40.6925 11.1878 40.4353 11.1878C39.037 11.1878 38.3378 12.2403 38.3378 14.3453V20.6354H33.974V14.3453C33.974 13.6989 33.9338 13.1685 33.8535 12.7541C33.7731 12.3232 33.6525 11.9834 33.4918 11.7348C33.3472 11.4862 33.1543 11.3204 32.9132 11.2376C32.6721 11.1381 32.3908 11.0884 32.0694 11.0884C31.7961 11.0884 31.539 11.1298 31.2979 11.2127C31.0568 11.2956 30.8398 11.4613 30.6469 11.7099C30.4701 11.942 30.3255 12.2735 30.2129 12.7044C30.1165 13.1188 30.0683 13.6657 30.0683 14.3453V20.6354H25.7045V7.88122Z" fill="#241636"/>
                            <path d="M52.0605 7.88122V20.6354H47.6967V7.88122H52.0605ZM47.4074 2.98343C47.4074 2.63536 47.4717 2.31216 47.6003 2.01381C47.7289 1.6989 47.9057 1.42541 48.1307 1.19337C48.3557 0.961325 48.6129 0.779005 48.9022 0.646409C49.2076 0.513812 49.529 0.447514 49.8666 0.447514C50.2041 0.447514 50.5175 0.513812 50.8068 0.646409C51.1122 0.779005 51.3774 0.961325 51.6024 1.19337C51.8275 1.42541 52.0043 1.6989 52.1328 2.01381C52.2614 2.31216 52.3257 2.63536 52.3257 2.98343C52.3257 3.33149 52.2614 3.66298 52.1328 3.9779C52.0043 4.27624 51.8275 4.54144 51.6024 4.77348C51.3774 5.00553 51.1122 5.18785 50.8068 5.32044C50.5175 5.45304 50.2041 5.51934 49.8666 5.51934C49.529 5.51934 49.2076 5.45304 48.9022 5.32044C48.6129 5.18785 48.3557 5.00553 48.1307 4.77348C47.9057 4.54144 47.7289 4.27624 47.6003 3.9779C47.4717 3.66298 47.4074 3.33149 47.4074 2.98343Z" fill="#241636"/>
                            <path d="M57.4776 0V20.6354H53.1138V0H57.4776Z" fill="#241636"/>
                            <path d="M62.7741 7.88122V27H58.4103V7.88122H62.7741ZM58.121 2.98343C58.121 2.63536 58.1853 2.31216 58.3139 2.01381C58.4425 1.6989 58.6193 1.42541 58.8443 1.19337C59.0693 0.961325 59.3265 0.779005 59.6158 0.646409C59.9212 0.513812 60.2426 0.447514 60.5801 0.447514C60.9177 0.447514 61.2311 0.513812 61.5204 0.646409C61.8258 0.779005 62.091 0.961325 62.316 1.19337C62.541 1.42541 62.7178 1.6989 62.8464 2.01381C62.975 2.31216 63.0393 2.63536 63.0393 2.98343C63.0393 3.33149 62.975 3.66298 62.8464 3.9779C62.7178 4.27624 62.541 4.54144 62.316 4.77348C62.091 5.00553 61.8258 5.18785 61.5204 5.32044C61.2311 5.45304 60.9177 5.51934 60.5801 5.51934C60.2426 5.51934 59.9212 5.45304 59.6158 5.32044C59.3265 5.18785 59.0693 5.00553 58.8443 4.77348C58.6193 4.54144 58.4425 4.27624 58.3139 3.9779C58.1853 3.66298 58.121 3.33149 58.121 2.98343Z" fill="#241636"/>
                            <path d="M72.8684 12.4061C72.7237 11.7762 72.4264 11.2707 71.9763 10.8895C71.5263 10.5083 70.9798 10.3177 70.3369 10.3177C69.6618 10.3177 69.1073 10.5 68.6733 10.8646C68.2555 11.2293 67.9902 11.7431 67.8777 12.4061H72.8684ZM67.7572 15.0166C67.7572 16.9558 68.6412 17.9254 70.4092 17.9254C71.3575 17.9254 72.0728 17.5276 72.5549 16.732H76.7741C75.9222 19.6492 73.7926 21.1077 70.3851 21.1077C69.3404 21.1077 68.384 20.9503 67.5161 20.6354C66.6482 20.3039 65.9008 19.8398 65.2739 19.2431C64.6632 18.6464 64.189 17.9337 63.8515 17.105C63.514 16.2762 63.3452 15.3481 63.3452 14.3204C63.3452 13.2597 63.5059 12.3066 63.8274 11.4613C64.1488 10.5994 64.6069 9.87016 65.2016 9.27348C65.7963 8.6768 66.5115 8.22099 67.3473 7.90608C68.1992 7.57459 69.1555 7.40884 70.2163 7.40884C71.2611 7.40884 72.2013 7.57459 73.0371 7.90608C73.8729 8.22099 74.5801 8.68508 75.1588 9.29834C75.7374 9.9116 76.1794 10.6657 76.4848 11.5608C76.7902 12.4392 76.9428 13.4337 76.9428 14.5442V15.0166H67.7572Z" fill="#241636"/>
                            <path d="M81.8723 0V20.6354H77.5085V0H81.8723Z" fill="#241636"/>
                            <path d="M87.2893 7.88122V20.6354H82.9255V7.88122H87.2893ZM82.6362 2.98343C82.6362 2.63536 82.7005 2.31216 82.8291 2.01381C82.9577 1.6989 83.1345 1.42541 83.3595 1.19337C83.5845 0.961325 83.8417 0.779005 84.131 0.646409C84.4364 0.513812 84.7578 0.447514 85.0954 0.447514C85.4329 0.447514 85.7463 0.513812 86.0356 0.646409C86.341 0.779005 86.6062 0.961325 86.8312 1.19337C87.0563 1.42541 87.2331 1.6989 87.3617 2.01381C87.4902 2.31216 87.5545 2.63536 87.5545 2.98343C87.5545 3.33149 87.4902 3.66298 87.3617 3.9779C87.2331 4.27624 87.0563 4.54144 86.8312 4.77348C86.6062 5.00553 86.341 5.18785 86.0356 5.32044C85.7463 5.45304 85.4329 5.51934 85.0954 5.51934C84.7578 5.51934 84.4364 5.45304 84.131 5.32044C83.8417 5.18785 83.5845 5.00553 83.3595 4.77348C83.1345 4.54144 82.9577 4.27624 82.8291 3.9779C82.7005 3.66298 82.6362 3.33149 82.6362 2.98343Z" fill="#241636"/>
                            <path d="M90.199 7.88122L93.1404 14.4199L96.1299 7.88122H101L94.5628 20.6354H91.6456L85.2807 7.88122H90.199Z" fill="#241636"/>
                        </svg>
                    </div>
                </div>
            </div>

            <div className="flex flex-col justify-between text-xs text-textGray">
                <div className="border-b-2 border-gray-200 font-medium uppercase tracking-wider">
                    <div className="flex flex-col-2 gap-8 pb-4">
                        <div className="flex flex-col w-1/2">
                            <p className="pb-2">{data.firstname}</p>
                            <input className="outline-none p-2 bg-backgroundGray border-gray-200 border-2 rounded-sm"></input>
                        </div>
                        <div className="flex flex-col w-1/2">
                            <p className="pb-2">{data.lastname}</p>
                            <input className="outline-none p-2 bg-backgroundGray border-gray-200 border-2 rounded-sm"></input>
                        </div>
                    </div>

                    <div className="flex flex-col-2 gap-8 pb-4">
                        <div className="flex flex-col w-1/2">
                            <p className="pb-2">{data.passwordconfirm}</p>
                            <input className="outline-none p-2 bg-backgroundGray border-gray-200 border-2 rounded-sm"></input>
                        </div>
                        <div className="flex flex-col w-1/2">
                            <p className="pb-2">{data.password}</p>
                            <input className="outline-none p-2 bg-backgroundGray border-gray-200 border-2 rounded-sm"></input>
                        </div>
                    </div>
                    
                    <div className="flex flex-col-2 gap-8 pb-4">
                        <div className="flex flex-col w-1/2">
                            <p className="pb-2">{data.email}</p>
                            <input className="outline-none p-2 bg-backgroundGray border-gray-200 border-2 rounded-sm"></input>
                        </div>

                        <div className="flex flex-col w-1/2">
                            <label className="pb-2">{data.gender}</label>
                            <div className="inline-block relative">
                                <select className="block tracking-wide appearance-none outline-none w-full p-2 bg-backgroundGray border-gray-200 border-2 rounded-sm">
                                    <option value="choice">Välj</option>
                                    <option value="female">Kvinna</option>
                                    <option value="male">Man</option>
                                    <option value="none">Inget av ovanstående</option>
                                </select>
                                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                    <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="flex flex-col-2 gap-8 pb-4">
                        <div className="flex flex-col w-1/2 pb-4">
                        <label className="pb-2">{data.role}</label>
                            <div className="inline-block relative">
                                <select className="block tracking-wide appearance-none outline-none w-full p-2 bg-backgroundGray border-gray-200 border-2 rounded-sm">
                                    <option value="choice">Förälder &amp; väntar barn</option>
                                    <option value="female">Väntar barn</option>
                                    <option value="par">Förälder</option>
                                    <option value="par">Planerar barn</option>
                                    <option value="par">Övrigt</option>
                                </select>
                                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                    <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                </div>
                            </div>                                                           
                        </div>
                        <div className="flex flex-col w-1/2">

                        </div>
                    </div>
                </div>

                <div className="flex flex-col pt-5">
                    <label className="uppercase tracking-wider pb-2">När väntar du barn?</label>
                        <div className="flex flex-row gap-4 pb-4">
                            <div className="inline-block relative">
                                <select className="block tracking-wide appearance-none outline-none w-full p-2 bg-backgroundGray border-gray-200 border-2 rounded-sm">
                                    <option value="year">År</option>
                                    <option value="twenty">2020</option>
                                    <option value="twentyone">2021</option>
                                </select>
                                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                    <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                </div> 
                            </div>

                            <div className="inline-block relative">
                                <select className="block tracking-wide appearance-none outline-none w-full p-2 bg-backgroundGray border-gray-200 border-2 rounded-sm">
                                    <option value="month">Månad</option>
                                    <option value="jan">Januari</option>
                                    <option value="feb">Februari</option>
                                    <option value="jan">Mars</option>
                                    <option value="feb">April</option>
                                    <option value="jan">Maj</option>
                                    <option value="feb">Juni</option>
                                    <option value="jan">Juli</option>
                                    <option value="feb">Augusti</option>
                                    <option value="jan">September</option>
                                    <option value="feb">Oktober</option>
                                    <option value="jan">November</option>
                                    <option value="feb">December</option>
                                </select> 
                                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                    <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                </div> 
                            </div>

                            <div className="inline-block relative">
                                <select className="block tracking-wide appearance-none outline-none w-full p-2 bg-backgroundGray border-gray-200 border-2 rounded-sm">
                                    <option value="day">Dag</option>
                                    <option value="one">1</option>
                                    <option value="two">2</option>
                                    <option value="thr">3</option>
                                    <option value="four">4</option>
                                    <option value="five">5</option>
                                    <option value="six">6</option>
                                    <option value="sev">7</option>
                                    <option value="eig">8</option>
                                    <option value="nine">9</option>
                                    <option value="ten">10</option>
                                    <option value="ele">11</option>
                                    <option value="twe">12</option>
                                    <option value="thi">13</option>
                                    <option value="fotee">14</option>
                                    <option value="fif">15</option>
                                    <option value="sexte">16</option>
                                    <option value="seventeen">17</option>
                                    <option value="eighte">18</option>
                                    <option value="ninete">19</option>
                                    <option value="twe">20</option>
                                    <option value="tweo">21</option>
                                    <option value="twet">22</option>
                                    <option value="tweth">23</option>
                                    <option value="twef">24</option>
                                    <option value="twefi">25</option>
                                    <option value="twes">26</option>
                                    <option value="twese">27</option>
                                    <option value="twee">28</option>
                                    <option value="twen">39</option>
                                    <option value="th">30</option>
                                    <option value="tho">31</option>
                                </select> 
                                <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                    <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                </div> 
                            </div>
                        </div>
                    </div>

                    <div className="uppercase tracking-wider pb-2">
                        <p className="text-textGray">Hur många barn har du?</p>
                    </div>
                    <div className="border-gray-200 border-2 rounded-md p-4">
                        <div className="flex flex-row justify-between uppercase tracking-wider pb-2 text-textGray">
                            <p>Barn 1</p>
                            <a className="text-buttonBlue" href="#">Ta bort</a>
                        </div>
                        <div className="flex flex-col">
                            <div className="flex flex-row gap-4">
                                <div className="inline-block relative">
                                    <select className="tracking-wide appearance-none cursor-pointer outline-none p-2 bg-backgroundGray border-gray-200 border-2 rounded-sm">
                                        <option value="year">År</option>
                                        <option value="twenty">2020</option>
                                        <option value="twentyone">2021</option>
                                    </select> 
                                    <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                    </div>
                                </div>

                                <div className="inline-block relative">
                                    <select className="tracking-wide appearance-none cursor-pointer outline-none p-2 bg-backgroundGray border-gray-200 border-2 rounded-sm">
                                        <option value="month">Månad</option>
                                        <option value="jan">Januari</option>
                                        <option value="feb">Februari</option>
                                        <option value="jan">Mars</option>
                                        <option value="feb">April</option>
                                        <option value="jan">Maj</option>
                                        <option value="feb">Juni</option>
                                        <option value="jan">Juli</option>
                                        <option value="feb">Augusti</option>
                                        <option value="jan">September</option>
                                        <option value="feb">Oktober</option>
                                        <option value="jan">November</option>
                                        <option value="feb">December</option>
                                    </select>
                                    <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                    </div>
                                </div>

                                <div className="inline-block relative">
                                    <select className="tracking-wide appearance-none cursor-pointer outline-none p-2 bg-backgroundGray border-gray-200 border-2 rounded-sm">
                                        <option value="day">Dag</option>
                                        <option value="one">1</option>
                                        <option value="two">2</option>
                                        <option value="thr">3</option>
                                        <option value="four">4</option>
                                        <option value="five">5</option>
                                        <option value="six">6</option>
                                        <option value="sev">7</option>
                                        <option value="eig">8</option>
                                        <option value="nine">9</option>
                                        <option value="ten">10</option>
                                        <option value="ele">11</option>
                                        <option value="twe">12</option>
                                        <option value="thi">13</option>
                                        <option value="fotee">14</option>
                                        <option value="fif">15</option>
                                        <option value="sexte">16</option>
                                        <option value="seventeen">17</option>
                                        <option value="eighte">18</option>
                                        <option value="ninete">19</option>
                                        <option value="twe">20</option>
                                        <option value="tweo">21</option>
                                        <option value="twet">22</option>
                                        <option value="tweth">23</option>
                                        <option value="twef">24</option>
                                        <option value="twefi">25</option>
                                        <option value="twes">26</option>
                                        <option value="twese">27</option>
                                        <option value="twee">28</option>
                                        <option value="twen">39</option>
                                        <option value="th">30</option>
                                        <option value="tho">31</option>
                                    </select> 
                                    <div className="pointer-events-none absolute inset-y-0 right-0 flex items-center px-2 text-gray-700">
                                        <svg className="fill-current h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M9.293 12.95l.707.707L15.657 8l-1.414-1.414L10 10.828 5.757 6.586 4.343 8z"/></svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <div className="flex flex-col w-1/3 py-3">
                    <ButtonLink
                    button={data.kidbutton}
                    appearance={getButtonAppearance(data.kidbutton.type, "light")}
                    compact
                    />
                </div>

                <div className="pb-3"> 
                    {/* interactive checkboxes below
                <input className="h-6" type="checkbox" id="vehicle1" name="vehicle1" value="Bike"></input>
                    <label for="vehicle1">Jag vill ta emot Familjelivs medlemsbrev</label> */}

                    <ul className="mt-4 flex flex-col gap-3">
                        <li
                        className="flex flex-row gap-2 items-center"
                        >
                            <MdCheckBox className="h-6 w-auto text-gray-900" />
                            <span><p>Jag vill ta emot Familjelivs medlemsbrev</p></span>
                        </li>
                        <li
                        className="flex flex-row gap-2 items-center"
                        >
                            <MdCheckBox className="h-6 w-auto text-gray-900" />
                            <span><p>Jag vill ta emot erbjudanden från våra partners</p></span>
                        </li>
                        <li
                        className="flex flex-row gap-2 items-center"
                        >
                            <MdCheckBox className="h-6 w-auto text-gray-900" />
                            <span><p>Jag har läst och förstått medlemsvillkoren. <a className="text-buttonBlue" href="#">Läs medlemsvillkoren</a></p></span>
                        </li>
                    </ul>
                </div>

                <div className="flex flex-row w-full justify-between pt-4">
                    {data.signupbuttons.map((button) => (
                        <ButtonLink
                        button={button}
                        appearance={getButtonAppearance(button.type, "light")}
                        key={button.id}
                        />
                    ))}
                </div>
            </div>
        </div>

        <div className="flex flex-row w-full gap-6 justify-center text-textGray pt-3">
            <div className="pt-2">
                <svg width="159" height="9" viewBox="0 0 159 9" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 8.53467V0H1.86211V6.88398H5.1208V8.53467H0Z" fill="#878093"/>
                    <path d="M109.756 8.53467C109.025 8.53467 108.406 8.41252 107.897 8.16822C107.388 7.91604 106.955 7.59294 106.598 7.19891L107.79 6.005C108.354 6.63545 109.049 6.95067 109.876 6.95067C110.321 6.95067 110.65 6.86004 110.865 6.67879C111.08 6.49754 111.187 6.25718 111.187 5.95772C111.187 5.72918 111.123 5.54005 110.996 5.39032C110.869 5.2327 110.611 5.12632 110.221 5.07115L109.399 4.96476C108.517 4.85444 107.869 4.59044 107.456 4.17277C107.051 3.7551 106.848 3.19951 106.848 2.50602C106.848 2.13564 106.919 1.79677 107.063 1.48943C107.206 1.18209 107.408 0.918087 107.67 0.697431C107.941 0.476775 108.266 0.307343 108.648 0.189134C109.037 0.0630448 109.478 0 109.971 0C110.599 0 111.151 0.0985072 111.628 0.295521C112.105 0.492536 112.514 0.780177 112.856 1.15844L111.652 2.36417C111.453 2.13564 111.211 1.95044 110.925 1.80859C110.646 1.65886 110.293 1.584 109.864 1.584C109.458 1.584 109.156 1.65492 108.958 1.79677C108.759 1.93862 108.66 2.13564 108.66 2.38781C108.66 2.67152 108.735 2.87641 108.886 3.0025C109.045 3.12859 109.3 3.21922 109.649 3.27438L110.472 3.40441C111.33 3.53838 111.966 3.80238 112.379 4.19641C112.792 4.58256 112.999 5.1342 112.999 5.85133C112.999 6.24536 112.927 6.60786 112.784 6.93885C112.641 7.26983 112.43 7.55353 112.152 7.78995C111.882 8.02637 111.544 8.21156 111.139 8.34553C110.734 8.47162 110.273 8.53467 109.756 8.53467Z" fill="#878093"/>
                    <path d="M13.4844 8.53467V7.05516H14.6412V1.4795H13.4844V0H17.7517V1.4795H16.5949V7.05516H17.7517V8.53467H13.4844Z" fill="#878093"/>
                    <path d="M124.14 8.53467L121.361 0H123.263L124.547 4.09615L125.227 6.77393H125.264L125.918 4.09615L127.202 0H129.043L126.239 8.53467H124.14Z" fill="#878093"/>
                    <path d="M69.5574 8.53467C68.9819 8.53467 68.4565 8.44404 67.9811 8.26278C67.5057 8.08153 67.1012 7.81359 66.7676 7.45897C66.434 7.09646 66.1755 6.65121 65.992 6.12321C65.8085 5.58733 65.7168 4.9687 65.7168 4.26733C65.7168 3.56596 65.8085 2.95128 65.992 2.42328C66.1755 1.8874 66.434 1.44215 66.7676 1.08752C67.1012 0.725013 67.5057 0.453133 67.9811 0.27188C68.4565 0.0906267 68.9819 0 69.5574 0C70.1329 0 70.6583 0.0906267 71.1337 0.27188C71.6091 0.453133 72.0135 0.725013 72.3471 1.08752C72.6807 1.44215 72.9393 1.8874 73.1228 2.42328C73.3063 2.95128 73.398 3.56596 73.398 4.26733C73.398 4.9687 73.3063 5.58733 73.1228 6.12321C72.9393 6.65121 72.6807 7.09646 72.3471 7.45897C72.0135 7.81359 71.6091 8.08153 71.1337 8.26278C70.6583 8.44404 70.1329 8.53467 69.5574 8.53467ZM69.5574 6.93885C70.1329 6.93885 70.5791 6.76154 70.896 6.40691C71.2212 6.05228 71.3839 5.54399 71.3839 4.88202V3.65265C71.3839 2.99068 71.2212 2.48238 70.896 2.12776C70.5791 1.77313 70.1329 1.59582 69.5574 1.59582C68.9819 1.59582 68.5316 1.77313 68.2063 2.12776C67.8894 2.48238 67.7309 2.99068 67.7309 3.65265V4.88202C67.7309 5.54399 67.8894 6.05228 68.2063 6.40691C68.5316 6.76154 68.9819 6.93885 69.5574 6.93885Z" fill="#878093"/>
                    <path d="M81.7637 8.53467V0H87.3112V1.65069H83.5849V3.39919H86.76V5.03765H83.5849V8.53467H81.7637Z" fill="#878093"/>
                    <path d="M26.1152 8.53467V0H32.0895V1.65069H28.0765V3.39919H31.4959V5.03765H28.0765V8.53467H26.1152Z" fill="#878093"/>
                    <path d="M137.408 8.53467V0H142.956V1.65069H139.183V3.39919H142.419V5.03765H139.183V6.88398H142.956V8.53467H137.408Z" fill="#878093"/>
                    <path d="M40.4551 8.53467V0H46.4293V1.65069H42.3668V3.39919H45.8508V5.03765H42.3668V6.88398H46.4293V8.53467H40.4551Z" fill="#878093"/>
                    <path d="M157.118 8.53467L156.523 6.50493H153.748L153.152 8.53467H151.318L154.057 0H156.296L159 8.53467H157.118ZM155.153 1.68737H155.093L154.176 4.92761H156.082L155.153 1.68737Z" fill="#878093"/>
                    <path d="M57.3534 3.96868C57.3534 4.67571 56.7802 5.24888 56.0732 5.24888C55.3661 5.24888 54.793 4.67571 54.793 3.96868C54.793 3.26164 55.3661 2.68848 56.0732 2.68848C56.7802 2.68848 57.3534 3.26164 57.3534 3.96868Z" fill="#878093"/>
                    <path d="M98.2342 3.96868C98.2342 4.67571 97.6611 5.24888 96.954 5.24888C96.247 5.24888 95.6738 4.67571 95.6738 3.96868C95.6738 3.26164 96.247 2.68848 96.954 2.68848C97.6611 2.68848 98.2342 3.26164 98.2342 3.96868Z" fill="#878093"/>
                </svg>
            </div>
            <div className="flex flex-row gap-4 text-xs pt-1">
                <a href="#">Support</a>
                <a href="#">Cookies</a>
                <a href="#">Villkor</a>
            </div>
        </div>
    </div>
  );
};

export default Signup;