const { colors } = require(`tailwindcss/defaultTheme`);

module.exports = {
  purge: ["./components/**/*.js", "./pages/**/*.js", "./articles/**/*.js"],
  theme: {
    borderRadius: {
      'none': '0',
      'sm': '0.125rem',
      default: '0.25rem',
      default: '4px',
      'md': '0.375rem',
      'lg': '0.5rem',
      'full': '9999px',
      'large': '21px',
    },
    extend: {
      colors: {
        primary: colors.blue,
        backgroundGray: "#F2F4F9",
        buttonBlue: "#54C0E8",
        textColor: "#241636",
        textGray: "#878093",
      },
      container: {
        center: true,
        padding: {
          default: "1rem",
          md: "2rem",
        },
      },
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
